defmodule MoBudget.MixProject do
  use Mix.Project

  def project do
    [
      app: :mobudget,
      version: "0.1.0",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {MoBudget.Application, []},
      extra_applications: [:logger, :runtime_tools, :os_mon]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.5.5"},
      {:phoenix_ecto, "~> 4.1"},
      {:ecto_sql, "~> 3.4"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_live_dashboard, "~> 0.2"},
      {:telemetry_metrics, "~> 0.4"},
      {:telemetry_poller, "~> 0.4"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      # GraphQL implementation for Elixir
      {:absinthe, "~> 1.5.0"},
      {:absinthe_plug, "~> 1.5.0"},
      {:absinthe_phoenix, "~> 2.0.0"},
      # Improve queries and avoid N+1
      {:dataloader, "~> 1.0.0"},
      # A custom Ecto type for storing encrypted passwords using Comeonin
      {:comeonin_ecto_password, "~> 3.0.0"},
      {:pbkdf2_elixir, "~> 1.0"},
      # Arbitrary precision decimal arithmetic
      {:decimal, "~> 1.0", override: true},
      # An Elixir Plug to add CORS
      {:cors_plug, "~> 1.5"},
      # A token based authentication library for use with Elixir applications.
      {:guardian, "~> 2.0"},
      # An extension to Guardian that tracks tokens in your application's database to prevent playback.
      {:guardian_db, "~> 2.0"},
      # An Ecto extension to support enums in your Ecto models.
      {:ecto_enum, "~> 1.4"},
      # An Erlang application that verifies the integrity of Google ID tokens in accordance with Google's criterias.
      {:google_token, "~> 1.0"},
      # An erlang application for consuming, producing and manipulating json. inspired by yajl
      {:jsx, "~> 3.0"},
      # A mocking library for the Elixir language.
      {:mock, "~> 0.3.0", only: :test},
      # An Elixir library that reports test coverage statistics
      {:excoveralls, "~> 0.10", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"]
    ]
  end
end
