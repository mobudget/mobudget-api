# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :mobudget,
  namespace: MoBudget,
  ecto_repos: [MoBudget.Repo]

# Configures migration options
config :mobudget, MoBudget.Repo,
  migration_primary_key: [
    name: :id,
    type: :binary_id,
    autogenerate: false,
    read_after_writes: true,
    default: {:fragment, "uuid_generate_v4()"}
  ],
  migration_timestamps: [type: :utc_datetime_usec]

# Configures the endpoint
config :mobudget, MoBudgetWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SQGUZBJi9MU8SLWEWHiPQI3/N2AIcureChjXZ/tnYRzqWOq8rDdId0M6nFwQ4KfC",
  render_errors: [view: MoBudgetWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: MoBudget.PubSub,
  live_view: [signing_salt: "Rb4YNXwA"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :mobudget, MoBudget.Auth,
  issuer: "mobudget",
  ttl: {7, :days},
  verify_issuer: true,
  secret_key: "QweiR11pPTt1eLtzou/v/kRD7EpD31H7uAz1FpFZnDAPg6Y/+tOzHdcTtl0IQDnV",
  serializer: MoBudget.Auth

config :guardian, Guardian.DB,
  repo: MoBudget.Repo,
  sweep_interval: 60

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
