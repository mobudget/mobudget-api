defmodule MoBudgetWeb.GraphQL.Plug.Context do
  @behaviour Plug

  import Plug.Conn

  alias MoBudget.Account

  def init(opts), do: opts

  def call(conn, _) do
    context = build_context(conn)
    Absinthe.Plug.put_options(conn, context: context)
  end

  defp build_context(conn) do
    with ["Bearer " <> token] <- get_req_header(conn, "authorization"),
         {:valid, claims} <- MoBudget.Auth.validate_google_id_token(token) do
      build_google_context(claims)
    else
      _ ->
        with ["Bearer " <> token] <- get_req_header(conn, "authorization"),
             {:ok, user, _claims} <- MoBudget.Auth.resource_from_token(token, %{typ: "access"}) do
          %{current_user: user}
        else
          _ -> %{}
        end
    end
  end

  defp build_google_context(%{"email" => email, "sub" => google_id} = claims) do
    if(user = Account.get_google_user(email, google_id)) do
      %{current_user: user}
    else
      %{google_claims: claims}
    end
  end
end
