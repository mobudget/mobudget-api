defmodule MoBudgetWeb.GraphQL.Resolver.Users do
  alias MoBudget.Account

  def create(_, %{input: params}, _) do
    with {:ok, user} <- Account.create_user(params) do
      {:ok, %{user: user, success: true}}
    end
  end

  def create_by_google(_, _, %{context: %{google_claims: claims}}) do
    with {:ok, user} <- Account.create_google_user(claims) do
      {:ok, %{user: user, success: true}}
    end
  end

  def login(_, %{email: email, password: password}, _) do
    with {:ok, user} <- Account.authenticate(email, password),
         {:ok, access_token, _} <- MoBudget.Auth.encode_and_sign(user),
         {:ok, _, {refresh_token, _}} <-
           MoBudget.Auth.exchange(access_token, "access", "refresh", ttl: {4, :weeks}) do
      {:ok, %{access_token: access_token, refresh_token: refresh_token}}
    else
      _ -> {:error, "incorrect email or password"}
    end
  end

  def refresh(_, %{refresh_token: refresh_token, access_token: access_token}, _) do
    if String.trim(to_string(access_token)) != "" do
      MoBudget.Auth.revoke(access_token)
    end

    with {:ok, _} <- MoBudget.Auth.decode_and_verify(refresh_token, %{typ: "refresh"}),
         {:ok, _, {new_refresh_token, _}} <-
           MoBudget.Auth.refresh(refresh_token, ttl: {4, :weeks}),
         {:ok, _, {new_access_token, _}} <-
           MoBudget.Auth.exchange(new_refresh_token, "refresh", "access") do
      {:ok, %{access_token: new_access_token, refresh_token: new_refresh_token}}
    else
      _ -> {:error, "invalid or expired refresh token"}
    end
  end

  def revoke(_, %{refresh_token: refresh_token, access_token: access_token}, _) do
    with _ <- MoBudget.Auth.revoke(access_token),
         _ <- MoBudget.Auth.revoke(refresh_token) do
      {:ok, %{result: "tokens revoked"}}
    end
  end

  def viewer(_, _, %{context: %{current_user: current_user}}) do
    {:ok, current_user}
  end
end
