defmodule MoBudgetWeb.GraphQL.Middleware.Authorize do
  @behaviour Absinthe.Middleware

  alias MoBudget.Account.User

  def call(resolution, :create_google_user) do
    with %{google_claims: %{}} <- resolution.context do
      resolution
    else
      _ ->
        with %{current_user: %User{}} <- resolution.context do
          resolution
            |> Absinthe.Resolution.put_result({:error, "already created"})
        else
          _ ->
            resolution
            |> Absinthe.Resolution.put_result({:error, "unauthorized"})
        end
    end
  end

  def call(resolution, _) do
    with %{current_user: %User{}} <- resolution.context do
      resolution
    else
      _ ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "unauthorized"})
    end
  end
end
