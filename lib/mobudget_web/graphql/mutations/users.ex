defmodule MoBudgetWeb.GraphQL.Mutation.Users do
  use Absinthe.Schema.Notation
  alias MoBudgetWeb.GraphQL.{Middleware, Resolver}

  object :users_mutations do
    field :create_user, :create_user_result do
      arg(:input, non_null(:create_user_input))
      resolve(&Resolver.Users.create/3)
    end

    field :create_google_user, :create_user_result do
      middleware(Middleware.Authorize, :create_google_user)
      resolve(&Resolver.Users.create_by_google/3)
    end

    field :login, :session do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))
      resolve(&Resolver.Users.login/3)
    end

    field :refresh, :session do
      arg(:refresh_token, non_null(:string))
      arg(:access_token, :string)
      resolve(&Resolver.Users.refresh/3)
    end

    field :revoke, :revoke_result do
      arg(:refresh_token, non_null(:string))
      arg(:access_token, non_null(:string))
      resolve(&Resolver.Users.revoke/3)
    end
  end
end
