defmodule MoBudgetWeb.GraphQL.Query.Users do
  use Absinthe.Schema.Notation
  alias MoBudgetWeb.GraphQL.{Middleware, Resolver}

  object :users_queries do
    field :viewer, :user do
      middleware(Middleware.Authorize)
      resolve(&Resolver.Users.viewer/3)
    end
  end
end
