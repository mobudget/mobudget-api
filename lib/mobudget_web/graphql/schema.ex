defmodule MoBudgetWeb.GraphQL.Schema do
  use Absinthe.Schema
  alias MoBudgetWeb.GraphQL.{Middleware, Mutation, Query, Scalar, Type}

  def middleware(middleware, field, object) do
    middleware
    |> apply(:errors, field, object)
  end

  defp apply(middleware, :errors, _field, %{identifier: :mutation}),
    do: middleware ++ [Middleware.HandleChangesetErrors]

  defp apply(middleware, _, _, _),
    do: middleware

  # Mutations
  import_types(Mutation.Users)

  # Queries
  import_types(Query.Users)

  # Scalar
  import_types(Scalar.UUID4)
  import_types(Absinthe.Type.Custom)

  # Types
  import_types(Type.Users)
  import_types(Type.Utils)

  query do
    import_fields(:users_queries)
  end

  mutation do
    import_fields(:users_mutations)
  end
end
