defmodule MoBudgetWeb.Router do
  use MoBudgetWeb, :router
  alias MoBudgetWeb.GraphQL

  pipeline :graphql do
    plug :accepts, ["json"]
    plug GraphQL.Plug.Context
  end

  scope "/" do
    pipe_through :graphql

    forward "/api", Absinthe.Plug, schema: GraphQL.Schema

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: GraphQL.Schema,
      socket: MoBudgetWeb.UserSocket
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: MoBudgetWeb.Telemetry
    end
  end
end
