defmodule MoBudget.Account do
  @moduledoc """
  The Account context.
  """

  import Ecto.Query, warn: false
  alias MoBudget.Repo
  alias Comeonin.Ecto.Password

  alias MoBudget.Account.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Gets a single google user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_google_user("email@example.com", "12345678")
      %User{}

      iex> get_google_user("email2@example.com", "12345678")
      nil

  """
  def get_google_user(email, google_id),
    do: Repo.get_by(User, email: email, google_id: google_id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.insert_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Creates a user by google login.

  ## Examples

      iex> create_google_user(%{field: value})
      {:ok, %User{}}

      iex> create_google_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_google_user(attrs \\ %{}) do
    attrs = Map.put(attrs, "google_id", attrs["sub"])
    attrs = for {key, val} <- attrs, into: %{}, do: {String.to_atom(key), val}

    %User{}
    |> User.google_insert_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  @doc """
  Authenicate and return use always is ok

  ## Examples

      iex> authenticate("foo@bar.com", "foobar")
      {:ok, %User{}}

      iex> authenticate("foo@bar.com", "wrong!")
      :error
  """
  def authenticate(email, password) do
    user = Repo.get_by(User, email: email)

    with %{password: digest} <- user,
         true <- Password.valid?(password, digest) do
      {:ok, user}
    else
      _ ->
        Pbkdf2.no_user_verify()
        :error
    end
  end
end
