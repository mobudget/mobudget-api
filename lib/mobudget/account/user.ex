defmodule MoBudget.Account.User do
  use MoBudget.Schema

  schema "users" do
    field :email, :string
    field :email_confirmation, :string, virtual: true
    field :google_id, :string
    field :name, :string
    field :password, Comeonin.Ecto.Password
    field :password_confirmation, Comeonin.Ecto.Password, virtual: true

    timestamps()
  end

  @required_fields ~w(name email password)a
  @confirmation_fields ~w(email_confirmation password_confirmation)a
  @google_user_fields ~w(name email google_id)a
  @email_regex ~r/^[\w.!#$%&’*+\-\/=?\^`{|}~]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/i

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
    |> update_change(:email, &String.downcase/1)
    |> validate_format(:email, @email_regex)
    |> unique_constraint(:email)
    |> validate_password(user, attrs)
  end

  @doc false
  def insert_changeset(user, attrs) do
    user
    |> cast(attrs, @required_fields ++ @confirmation_fields)
    |> validate_required(@required_fields ++ @confirmation_fields)
    |> update_change(:email, &String.downcase/1)
    |> validate_format(:email, @email_regex)
    |> validate_confirmation(:email)
    |> validate_confirmation(:password)
    |> unique_constraint(:email)
    |> validate_password(user, attrs)
  end

  @doc false
  def google_insert_changeset(user, attrs) do
    user
    |> cast(attrs, @google_user_fields)
    |> validate_required(@google_user_fields)
    |> update_change(:email, &String.downcase/1)
    |> validate_format(:email, @email_regex)
    |> unique_constraint(:email)
  end

  @doc false
  def validate_password(changeset, user, attrs) do
    {user, %{password: :string}}
    |> cast(attrs, [:password])
    |> validate_length(:password, min: 8)
    |> merge(changeset)
  end
end
