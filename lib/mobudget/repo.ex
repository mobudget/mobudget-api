defmodule MoBudget.Repo do
  use Ecto.Repo,
    otp_app: :mobudget,
    adapter: Ecto.Adapters.Postgres
end
