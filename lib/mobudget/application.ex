defmodule MoBudget.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      MoBudget.Repo,
      # To sweep expired tokens from db
      Guardian.DB.Token.SweeperServer,
      # Start the Telemetry supervisor
      MoBudgetWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: MoBudget.PubSub},
      # Start the Endpoint (http/https)
      MoBudgetWeb.Endpoint,
      # Start Absinthe.Subscription
      {Absinthe.Subscription, MoBudgetWeb.Endpoint}
      # Start a worker by calling: MoBudget.Worker.start_link(arg)
      # {MoBudget.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MoBudget.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MoBudgetWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
