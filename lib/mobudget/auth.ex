defmodule MoBudget.Auth do
  use Guardian, otp_app: :mobudget

  alias MoBudget.Account

  def validate_google_id_token(token) do
    client_id = "140333703233-112hl5e6a7c59qj5u0rqokq37848oc9o.apps.googleusercontent.com"
    :google_token.validate(token, [client_id])
  end

  def subject_for_token(user, _claims) do
    {:ok, to_string(user.id)}
  end

  def resource_from_claims(%{"sub" => id}) do
    {:ok, Account.get_user!(id)}
  rescue
    Ecto.NoResultsError -> {:error, :resource_not_found}
  end

  def after_encode_and_sign(resource, claims, token, _options) do
    with {:ok, _} <- Guardian.DB.after_encode_and_sign(resource, claims["typ"], claims, token) do
      {:ok, token}
    end
  end

  def on_exchange(old_token_and_claims, {new_token, new_claims} = new_token_and_claims, _options) do
    with {:ok, _} <-
           Guardian.DB.after_encode_and_sign(%{}, new_claims["typ"], new_claims, new_token) do
      {:ok, old_token_and_claims, new_token_and_claims}
    end
  end

  def on_verify(claims, token, _options) do
    with {:ok, _} <- Guardian.DB.on_verify(claims, token) do
      {:ok, claims}
    end
  end

  def on_refresh({old_token, old_claims}, {new_token, new_claims}, _options) do
    with {:ok, _, _} <- Guardian.DB.on_refresh({old_token, old_claims}, {new_token, new_claims}) do
      {:ok, {old_token, old_claims}, {new_token, new_claims}}
    end
  end

  def on_revoke(claims, token, _options) do
    with {:ok, _} <- Guardian.DB.on_revoke(claims, token) do
      {:ok, claims}
    end
  end
end
